// to build the custom slider

const slides = document.querySelectorAll('.slide')
let counter = 0
// console.log(slides)
slides.forEach((slide, index) => {
  //callback
  slide.style.left = `${index * 100}%`
})
// to go to next img
const goNext = () => {
  if (counter < slides.length - 1) {
    counter++
    slideImage()
  }
}
// to go to prev image
const goPrev = () => {
  if (counter != 0) {
    counter--
    slideImage()
  }
}
const slideImage = () => {
  slides.forEach((slide) => {
    slide.style.transform = `translateX(-${counter * 100}%)`
  })
}
